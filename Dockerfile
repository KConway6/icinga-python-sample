FROM python:3.8-slim-buster
RUN pip install flask
RUN pip install flask_restful
WORKDIR /app
COPY main.py .
EXPOSE 5000
CMD ["python", "main.py"]
