from flask import Flask
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)

things = [
  {
    'id': 1,
    'name': 'alskdjfelkaf'
  },
  {
    'id': 2,
    'name': 'jfjfjfjfjf'
  }
]

numThings = int(things[-1]['id']) + 1

class Things(Resource):
  def get(self):
    return things
  
  def post(self):
    global numThings
    parser = reqparse.RequestParser()
    parser.add_argument('name', required=True)
    args = parser.parse_args()
    
    thingData = {
      'id': numThings,
      'name': args['name']
    }
    
    things.append(thingData)
    numThings = int(things[-1]['id']) + 1
    return things
  
  def put(self):
    global numThings
    parser = reqparse.RequestParser()
    parser.add_argument('id', required=True)
    parser.add_argument('name', required=True)
    args = parser.parse_args()
    
    for x in things:
      if int(args['id']) == x['id']:
        things[int(args['id']) - 1]['name'] = args['name']
    return things
  
  def delete(self):
    parser = reqparse.RequestParser()
    parser.add_argument('id', required=True)
    args = parser.parse_args()
    
    for x in things:
      if int(args['id']) == x['id']:
        things.pop(int(args['id']) - 1)
    
    return things

# /things
api.add_resource(Things, '/things')

if __name__ == "__main__":
  app.run(host="0.0.0.0", port=5000)